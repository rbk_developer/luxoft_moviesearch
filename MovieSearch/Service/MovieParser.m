//
//  MovieParser.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieParser.h"

@interface MovieParser ()
@end

@implementation MovieParser

- (Movie *)parse:(NSDictionary *)movieJson {
    Movie *movie = [Movie new];
    movie.movieID = [movieJson[@"id"] integerValue];
    movie.title = movieJson[@"title"];
    movie.posterPath = movieJson[@"poster_path"];
    movie.backdropPath = movieJson[@"backdrop_path"];
    movie.releaseDate = movieJson[@"release_date"];
    movie.overview = movieJson[@"overview"];
    movie.voteAverage = movieJson[@"vote_average"];
    movie.voteCount = movieJson[@"vote_count"];
    return movie;
}

@end
