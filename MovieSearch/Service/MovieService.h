//
//  MovieService.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import "Movie.h"
#import "MovieParser.h"

typedef void (^LoadMoviesCompletionBlock)(NSArray<Movie *>*, BOOL hasMorePages);
typedef void (^LoadKeywordsCompletionBlock)(NSArray<NSString *>*);
typedef void (^LoadPosterCompletionBlock)(NSData *);

@protocol MovieServiceProtocol <NSObject>

- (void)loadMoviesListWithPage:(NSInteger)page completion:(LoadMoviesCompletionBlock)completion;
- (void)searchForKeywordsWithQuery:(NSString *)query completion:(LoadKeywordsCompletionBlock)completion;
- (void)searchForMoviesWithQuery:(NSString *)query page:(NSInteger)page completion:(LoadMoviesCompletionBlock)completion;
- (void)loadMovieBackdrop:(Movie *)movie completion:(LoadPosterCompletionBlock)completion;

@end

@interface MovieService : NSObject<MovieServiceProtocol>

- (id)initWithParser:(id<MovieParserProtocol>)movieParser;

@end
