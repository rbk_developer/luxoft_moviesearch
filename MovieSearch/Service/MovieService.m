//
//  MovieService.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieService.h"

const NSString *APIKey = @"a423f848e68277e3fedf6b0dfa247602";
const NSString *BaseURL = @"https://api.themoviedb.org/3";
const NSString *ImagesURL = @"https://image.tmdb.org/t/p/w500";

@interface MovieService ()
@property (nonatomic, strong) id<MovieParserProtocol> movieParser;
@end

@implementation MovieService

@synthesize movieParser;

- (id)initWithParser:(id<MovieParserProtocol>)movieParser {
    if (self = [super init]) {
        self.movieParser = movieParser;
    }
    return self;
}

- (void)loadMoviesListWithPage:(NSInteger)page completion:(LoadMoviesCompletionBlock)completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[self urlStringWithEndpoint:@"movie/now_playing" params:[NSString stringWithFormat:@"&page=%ld", (long)page]]]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSError *parsingError = nil;
            NSDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parsingError];
            NSArray *moviesJson = responseJson[@"results"];
            NSMutableArray<Movie *> *movies = [NSMutableArray new];
            for (NSDictionary *movieJson in moviesJson) {
                [movies addObject:[self.movieParser parse: movieJson]];
            }
            NSNumber *totalPages = responseJson[@"total_pages"];
            completion(movies, totalPages.integerValue > page);
        } else {
            completion(nil, false);
        }
    }] resume];
}

- (void)searchForKeywordsWithQuery:(NSString *)query completion:(LoadKeywordsCompletionBlock)completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[self urlStringWithEndpoint:@"search/keyword" params:[NSString stringWithFormat:@"&query=%@", [query stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet]]]]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSError *parsingError = nil;
            NSDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parsingError];
            NSArray *keywordsJson = responseJson[@"results"];
            NSMutableArray<NSString *> *keywords = [NSMutableArray new];
            for (NSDictionary *keywordJson in keywordsJson) {
                [keywords addObject:keywordJson[@"name"]];
            }
            
            completion(keywords);
        } else {
            completion(nil);
        }
    }] resume];
}

- (void)searchForMoviesWithQuery:(NSString *)query page:(NSInteger)page completion:(LoadMoviesCompletionBlock)completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[self urlStringWithEndpoint:@"search/movie" params:[NSString stringWithFormat:@"&query=%@&page=%ld", [query stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet], (long)page]]]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSError *parsingError = nil;
            NSDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parsingError];
            NSArray *moviesJson = responseJson[@"results"];
            NSMutableArray<Movie *> *movies = [NSMutableArray new];
            for (NSDictionary *movieJson in moviesJson) {
                [movies addObject:[self.movieParser parse: movieJson]];
            }
            NSNumber *totalPages = responseJson[@"total_pages"];
            completion(movies, totalPages.integerValue > page);
        } else {
            completion(nil, false);
        }
    }] resume];
}

- (void)loadMovieBackdrop:(Movie *)movie completion:(LoadPosterCompletionBlock)completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImagesURL, movie.backdropPath]]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            completion(data);
        } else {
            completion(nil);
        }
    }] resume];
}

- (NSString *)urlStringWithEndpoint:(NSString *)endpoint params:(NSString *)params {
    return [NSString stringWithFormat:@"%@/%@?api_key=%@%@", BaseURL, endpoint, APIKey, params];
}

@end
