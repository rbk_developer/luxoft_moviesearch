//
//  MovieParser.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import "Movie.h"

@protocol MovieParserProtocol <NSObject>

- (Movie *)parse:(NSDictionary *)movieJson;

@end

@interface MovieParser : NSObject<MovieParserProtocol>


@end
