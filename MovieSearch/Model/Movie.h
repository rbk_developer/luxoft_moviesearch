//
//  Movie.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

@interface Movie : NSObject

@property (nonatomic) NSInteger movieID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *posterPath;
@property (nonatomic, strong) NSString *backdropPath;
@property (nonatomic, strong) NSString *releaseDate;
@property (nonatomic, strong) NSString *overview;
@property (nonatomic, strong) NSNumber *voteCount;
@property (nonatomic, strong) NSNumber *voteAverage;
@property (nonatomic) BOOL isFavorite;

@end
