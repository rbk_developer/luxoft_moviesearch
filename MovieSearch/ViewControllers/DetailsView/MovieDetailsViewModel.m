//
//  MovieDetailsViewModel.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieDetailsViewModel.h"

@interface MovieDetailsViewModel ()
@property (nonatomic, strong) id<MovieServiceProtocol> movieService;
@property (nonatomic, strong) id<MovieStorageProtocol> storage;
@property (nonatomic, strong) Movie *movie;
@end

@implementation MovieDetailsViewModel

@synthesize onImageFetched;
@synthesize onIsFavoriteChanged;

- (id)initWithMovie:(Movie *)movie movieService:(id<MovieServiceProtocol>)movieService storage:(id<MovieStorageProtocol>)storage {
    if (self = [super init]) {
        self.movieService = movieService;
        self.storage = storage;
        self.movie = movie;
    }
    return self;
}

- (void)viewDidLoad {
    if (self.backdropImageData == nil && self.movie.backdropPath != [NSNull null]) {
        __weak MovieDetailsViewModel *weakSelf = self;
        [self.movieService loadMovieBackdrop:self.movie completion:^(NSData *imageData){
            dispatch_async(dispatch_get_main_queue(), ^() {
                [weakSelf.storage saveimageData:imageData movie:weakSelf.movie];
                weakSelf.onImageFetched(imageData);
            });
        }];
    }
}

- (void)didToggleFavorite {
    if (self.movie.isFavorite) {
        [self.storage removeMovieFromFavorites:self.movie];
    } else {
        [self.storage addMovieToFavorites:self.movie];
    }
    if (self.onIsFavoriteChanged) {
        self.onIsFavoriteChanged(self.isFavorite);
    }
}

#pragma mark - Getters

- (NSData *)backdropImageData {
    return [self.storage imageDataWithMovie:self.movie];
}

- (BOOL)isFavorite {
    return [self.storage isMovieFavorite:self.movie];
}

- (NSString *)title {
    return self.movie.title;
}

- (NSString *)releaseDate {
    return self.movie.releaseDate;
}

- (NSString *)votesText {
    if (!self.movie.voteAverage || !self.movie.voteCount) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@ (%@)", self.movie.voteAverage, self.movie.voteCount];
}

- (NSString *)overview {
    return self.movie.overview;
}

@end
