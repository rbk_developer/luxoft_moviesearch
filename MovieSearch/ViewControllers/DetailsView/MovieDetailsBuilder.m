//
//  MovieDetailsBuilder.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieDetailsBuilder.h"
#import "MovieDetailsViewController.h"
#import "MovieDetailsViewModel.h"
#import "MovieService.h"
#import "MovieParser.h"
#import "MovieStorage.h"

@interface MovieDetailsBuilder ()
@end

@implementation MovieDetailsBuilder

- (UIViewController *)movieDetailsViewControllerWithMovie:(Movie *)movie {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MovieDetailsViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MovieDetailsViewController"];
    MovieService *movieService = [[MovieService alloc] initWithParser: [MovieParser new]];
    viewController.viewModel = [[MovieDetailsViewModel alloc] initWithMovie:movie movieService: movieService storage: [MovieStorage sharedInstance]];
    return viewController;
}

@end
