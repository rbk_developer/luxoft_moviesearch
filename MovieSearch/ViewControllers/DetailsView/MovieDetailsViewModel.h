//
//  MovieDetailsViewModel.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import "MovieService.h"
#import "MovieStorage.h"
#import "Movie.h"

typedef void (^MovieDetailsViewImageFetchedBlock)(NSData *data);
typedef void (^MovieDetailsViewIsFavoriteChangedBlock)(BOOL isFavorite);

@protocol MovieDetailsViewModelProtocol <NSObject>

@property (nonatomic, strong) MovieDetailsViewImageFetchedBlock onImageFetched;
@property (nonatomic, strong) MovieDetailsViewIsFavoriteChangedBlock onIsFavoriteChanged;
@property (nonatomic, readonly) NSData *backdropImageData;
@property (nonatomic, readonly) BOOL isFavorite;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *releaseDate;
@property (nonatomic, readonly) NSString *votesText;
@property (nonatomic, readonly) NSString *overview;

- (void)viewDidLoad;
- (void)didToggleFavorite;

@end

@interface MovieDetailsViewModel : NSObject<MovieDetailsViewModelProtocol>

- (id)initWithMovie:(Movie *)movie movieService:(id<MovieServiceProtocol>)movieService storage:(id<MovieStorageProtocol>)storage;

@end
