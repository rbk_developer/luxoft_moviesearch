//
//  MovieDetailsViewController.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 05/10/2020.
//

#import <UIKit/UIKit.h>
#import "MovieDetailsViewModel.h"

@interface MovieDetailsViewController : UIViewController

@property (nonatomic, strong) id<MovieDetailsViewModelProtocol> viewModel;

@end

