//
//  MovieDetailsBuilder.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@protocol MovieDetailsBuilderProtocol <NSObject>

- (UIViewController *)movieDetailsViewControllerWithMovie:(Movie *)movie;

@end

@interface MovieDetailsBuilder : NSObject <MovieDetailsBuilderProtocol>

@end
