//
//  MovieDetailsViewController.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 05/10/2020.
//

#import "MovieDetailsViewController.h"

@interface MovieDetailsViewController ()
@property (nonatomic, weak) IBOutlet UIImageView *backdropImageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *releaseDateLabel;
@property (nonatomic, weak) IBOutlet UILabel *votesLabel;
@property (nonatomic, weak) IBOutlet UILabel *overviewLabel;
@property (nonatomic, weak) IBOutlet UIImageView *isFavoriteView;
@end

@implementation MovieDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.viewModel viewDidLoad];
    if (self.viewModel.backdropImageData) {
        self.backdropImageView.image = [UIImage imageWithData:self.viewModel.backdropImageData];
    }
    self.titleLabel.text = self.viewModel.title;
    self.releaseDateLabel.text = self.viewModel.releaseDate;
    self.votesLabel.text = self.viewModel.votesText;
    self.overviewLabel.text = self.viewModel.overview;
    self.isFavoriteView.image = self.viewModel.isFavorite ? [UIImage imageNamed:@"favorite_on"] : [UIImage imageNamed:@"favorite_off"];
    [self bindToViewModel];
}

- (void)bindToViewModel {
    __weak MovieDetailsViewController *weakSelf = self;
    self.viewModel.onImageFetched = ^(NSData *imageData) {
        if (imageData) {
            weakSelf.backdropImageView.image = [UIImage imageWithData:imageData];
        }
    };
    self.viewModel.onIsFavoriteChanged = ^(BOOL isFavorite) {
        weakSelf.isFavoriteView.image = isFavorite ? [UIImage imageNamed:@"favorite_on"] : [UIImage imageNamed:@"favorite_off"];
    };
}

- (IBAction)didToggleFavorite:(id)sender {
    [self.viewModel didToggleFavorite];
}

@end
