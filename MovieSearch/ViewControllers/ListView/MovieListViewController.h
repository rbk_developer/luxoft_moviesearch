//
//  MovieListViewController.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 05/10/2020.
//

#import <UIKit/UIKit.h>
#import "MovieListViewModel.h"
#import "MovieDetailsBuilder.h"

@interface MovieListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) id<MovieListViewModelProtocol> viewModel;
@property (nonatomic, strong) id<MovieDetailsBuilderProtocol> movieDetailsBuilder;

@end

