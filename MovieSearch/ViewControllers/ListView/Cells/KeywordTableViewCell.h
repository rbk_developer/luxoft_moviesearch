//
//  KeywordTableViewCell.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 09/10/2020.
//

#import <UIKit/UIKit.h>

@interface KeywordTableViewCell : UITableViewCell
- (void)setupWithKeyword:(NSString *)keyword;
@end
