//
//  MovieTableViewCell.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <UIKit/UIKit.h>

typedef void (^DidToggleFavoriteBlock)(void);

@interface MovieTableViewCell : UITableViewCell
@property (nonatomic, strong) DidToggleFavoriteBlock onFavoriteToggled;
- (void)setupWithTitle:(NSString *)title logo:(UIImage *)logo isFavorite:(BOOL)isFavorite;
@end
