//
//  MovieTableViewCell.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <UIKit/UIKit.h>
#import "MovieTableViewCell.h"

@interface MovieTableViewCell ()
@property (nonatomic, weak) IBOutlet UIImageView *logoView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *isFavoriteView;
@property (nonatomic, weak) IBOutlet UIView *isFavoriteTappableView;
@end

@implementation MovieTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib]; 
    [self.isFavoriteTappableView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didToggleFavorite:)]];
}

- (void)setupWithTitle:(NSString *)title logo:(UIImage *)logo isFavorite:(BOOL)isFavorite {
    self.titleLabel.text = title;
    self.logoView.image = logo;
    self.isFavoriteView.image = isFavorite ? [UIImage imageNamed:@"favorite_on"] : [UIImage imageNamed:@"favorite_off"];
}

- (void)didToggleFavorite:(id)sender {
    if (self.onFavoriteToggled) {
        self.onFavoriteToggled();
    }
}

@end
