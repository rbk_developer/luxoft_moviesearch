//
//  KeywordTableViewCell.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 09/10/2020.
//

#import <UIKit/UIKit.h>
#import "KeywordTableViewCell.h"

@interface KeywordTableViewCell ()
@property (nonatomic, weak) IBOutlet UILabel *keywordLabel;
@end

@implementation KeywordTableViewCell

- (void)setupWithKeyword:(NSString *)keyword {
    self.keywordLabel.text = keyword;
}

@end
