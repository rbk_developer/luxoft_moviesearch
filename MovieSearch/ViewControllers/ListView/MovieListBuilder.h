//
//  MovieListBuilder.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <UIKit/UIKit.h>

@interface MovieListBuilder : NSObject

- (UIViewController *)movieListViewController;

@end
