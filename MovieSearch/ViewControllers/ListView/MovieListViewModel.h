//
//  MovieListViewModel.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import "MovieService.h"
#import "MovieStorage.h"

typedef NS_ENUM(NSInteger, MovieListViewModelContentType) {
    MovieListViewModelContentTypeNowPlaying,
    MovieListViewModelContentTypeKeywords,
    MovieListViewModelContentTypeSearch
};

@interface MovieListViewContentItem : NSObject
@property (nonatomic) MovieListViewModelContentType contentType;
@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, strong) NSData *imageData;
@end

typedef void (^MovieListViewContentChangedBlock)(NSArray<MovieListViewContentItem *>* newContent);
typedef void (^MovieListViewLoadingChangedBlock)(BOOL isLoading);
typedef void (^MovieListViewImageFetchedBlock)(NSInteger itemIndex);
typedef void (^MovieListViewHasMorePagesBlock)(BOOL hasMorePages);
typedef void (^MovieListViewSearchQueryBlock)(NSString *searchQuery);
typedef void (^MovieListViewMovieDetailsShouldBeDisplayedBlock)(Movie *movie);

@protocol MovieListViewModelProtocol <NSObject>

@property (nonatomic, readonly) MovieListViewModelContentType contentType;
@property (nonatomic, readonly) BOOL isLoading;
@property (nonatomic, readonly) NSString *currentSearchQuery;
@property (nonatomic, strong) MovieListViewContentChangedBlock onContentChanged;
@property (nonatomic, strong) MovieListViewLoadingChangedBlock onLoadingChanged;
@property (nonatomic, strong) MovieListViewImageFetchedBlock onImageFetched;
@property (nonatomic, strong) MovieListViewSearchQueryBlock onSearchQueryChanged;
@property (nonatomic, strong) MovieListViewHasMorePagesBlock onHasMorePagesChanged;
@property (nonatomic, strong) MovieListViewMovieDetailsShouldBeDisplayedBlock onMovieDetailsShouldBeDisplayed;

- (void)viewDidLoad;
- (void)didToggleFavorite:(MovieListViewContentItem *)item;
- (void)willDisplayItem:(MovieListViewContentItem *)item;
- (void)didSelectItem:(MovieListViewContentItem *)item;
- (void)didToggleNextPage;
- (void)didEnterSearchQuery:(NSString *)query;
- (void)didTapSearchButtonWithQuery:(NSString *)query;
- (void)didTapCancelSearchButton;

@end

@interface MovieListViewModel : NSObject<MovieListViewModelProtocol>

- (id)initWithMovieService:(id<MovieServiceProtocol>)movieService storage:(id<MovieStorageProtocol>)storage;

@end
