//
//  MovieListViewModel.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieListViewModel.h"
#import "Movie.h"

const NSInteger MinSearchQueryChars = 2;

@interface MovieListViewModel ()
@property (nonatomic, strong) id<MovieServiceProtocol> movieService;
@property (nonatomic, strong) id<MovieStorageProtocol> storage;
@property (nonatomic, strong) NSArray<MovieListViewContentItem *> *content;
@property (nonatomic, strong) NSArray<MovieListViewContentItem *> *playingNowContent;
@property (nonatomic, strong) NSMutableArray<NSNumber *> *loadingImagesMovieIDs;
@property (nonatomic, strong) NSString *currentSearchQuery;
@property (nonatomic) MovieListViewModelContentType contentType;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL hasMorePages;
@property (nonatomic) NSInteger contentPage;
@property (nonatomic) NSInteger playingNowContentPage;
@end

@implementation MovieListViewModel

@synthesize content = _content;
@synthesize contentType = _contentType;
@synthesize isLoading = _isLoading;
@synthesize movieService;
@synthesize onContentChanged;
@synthesize onLoadingChanged;
@synthesize onImageFetched;
@synthesize onHasMorePagesChanged;
@synthesize onMovieDetailsShouldBeDisplayed;
@synthesize hasMorePages = _hasMorePages;
@synthesize currentSearchQuery = _currentSearchQuery;
@synthesize onSearchQueryChanged;

- (id)initWithMovieService:(id<MovieServiceProtocol>)movieService storage:(id<MovieStorageProtocol>)storage {
    if (self = [super init]) {
        self.movieService = movieService;
        self.storage = storage;
        self.loadingImagesMovieIDs = [NSMutableArray new];
    }
    return self;
}

- (void)viewDidLoad {
    self.playingNowContent = @[];
    self.playingNowContentPage = 1;
    self.contentType = MovieListViewModelContentTypeNowPlaying;
    [self loadNowPlayingPage];
}

- (void)didToggleFavorite:(MovieListViewContentItem *)item {
    if (item.movie.isFavorite) {
        [self.storage removeMovieFromFavorites:item.movie];
    } else {
        [self.storage addMovieToFavorites:item.movie];
    }
    self.content = self.content;
}

- (void)willDisplayItem:(MovieListViewContentItem *)item {
    if (item.movie != nil && !item.imageData && item.movie.backdropPath != [NSNull null] && ![self.loadingImagesMovieIDs containsObject: @(item.movie.movieID)]) {
        [self.loadingImagesMovieIDs addObject:@(item.movie.movieID)];
        __weak MovieListViewModel *weakSelf = self;
        [self.movieService loadMovieBackdrop:item.movie completion:^(NSData *data) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                item.imageData = data;
                [weakSelf.storage saveimageData:data movie:item.movie];
                [weakSelf.loadingImagesMovieIDs removeObject: @(item.movie.movieID)];
                if (weakSelf.onImageFetched) {
                    weakSelf.onImageFetched([weakSelf.content indexOfObject: item]);
                }
            });
        }];
    }
}

- (void)didSelectItem:(MovieListViewContentItem *)item {
    if (item.movie && self.onMovieDetailsShouldBeDisplayed) {
        self.onMovieDetailsShouldBeDisplayed(item.movie);
    } else if (item.keyword) {
        self.currentSearchQuery = item.keyword;
        self.contentType = MovieListViewModelContentTypeSearch;
        [self searchForMovies:item.keyword];
    }
}

- (void)didToggleNextPage {
    if (self.isLoading) {
        return;
    }
    self.contentPage += 1;
    switch (self.contentType) {
        case MovieListViewModelContentTypeNowPlaying:
            [self loadNowPlayingPage];
            self.playingNowContentPage = self.contentPage;
            break;
        default:
            break;
    }
}

- (void)loadNowPlayingPage {
    __weak MovieListViewModel *weakSelf = self;
    self.isLoading = true;
    [self.movieService loadMoviesListWithPage:self.contentPage completion:^(NSArray<Movie *> *movies, BOOL hasMorePages) {
        if (weakSelf == nil) {
            return;
        }
        [weakSelf didLoadMovies:movies contentType:MovieListViewModelContentTypeNowPlaying];
        self.hasMorePages = hasMorePages;
        weakSelf.isLoading = false;
    }];
}

- (void)didLoadMovies:(NSArray<Movie *> *)movies contentType:(MovieListViewModelContentType)contentType {
    if (contentType == self.contentType) {
        NSMutableArray<MovieListViewContentItem *> *contentItems = [_content mutableCopy];
        for (Movie *movie in movies) {
            movie.isFavorite = [self.storage isMovieFavorite:movie];
            MovieListViewContentItem *item = [MovieListViewContentItem new];
            item.contentType = contentType;
            item.movie = movie;
            item.imageData = [self.storage imageDataWithMovie:movie];
            [contentItems addObject:item];
        }
        self.content = contentItems;
        if (contentType == MovieListViewModelContentTypeNowPlaying) {
            self.playingNowContent = contentItems;
        }
    }
}

- (void)didLoadKeywords:(NSArray<NSString *> *)keywords {
    if (self.contentType == MovieListViewModelContentTypeKeywords) {
        NSMutableArray<MovieListViewContentItem *> *contentItems = [_content mutableCopy];
        for (NSString *keyword in keywords) {
            MovieListViewContentItem *item = [MovieListViewContentItem new];
            item.contentType = MovieListViewModelContentTypeKeywords;
            item.keyword = keyword;
            [contentItems addObject:item];
        }
        self.content = contentItems;
    }
}

#pragma mark - Search

- (void)didEnterSearchQuery:(NSString *)query {
    if ([query isEqualToString:self.currentSearchQuery]) {
        return;
    }
    self.currentSearchQuery = query;
    if (query.length == 0) {
        self.contentType = MovieListViewModelContentTypeNowPlaying;
    } else {
        self.contentType = MovieListViewModelContentTypeKeywords;
        self.hasMorePages = NO;
        if (query.length >= MinSearchQueryChars) {
            [self searchForKeywords:query];
        }
    }
}

- (void)didTapCancelSearchButton {
    self.contentType = MovieListViewModelContentTypeNowPlaying;
    self.currentSearchQuery = nil;
}

- (void)didTapSearchButtonWithQuery:(NSString *)query {
    self.currentSearchQuery = query;
    if (query.length > 0) {
        self.contentType = MovieListViewModelContentTypeSearch;
        [self searchForMovies:query];
    }
}

- (void)searchForMovies:(NSString *)query {
    self.isLoading = true;
    __weak MovieListViewModel *weakSelf = self;
    [self.movieService searchForMoviesWithQuery:query page:self.contentPage completion:^(NSArray<Movie *> *movies, BOOL hasMorePages) {
        if (weakSelf == nil || ![weakSelf.currentSearchQuery isEqualToString:query]) {
            return;
        }
        [weakSelf didLoadMovies:movies contentType:MovieListViewModelContentTypeSearch];
        self.hasMorePages = hasMorePages;
        weakSelf.isLoading = false;
    }];
}

- (void)searchForKeywords:(NSString *)query {
    self.isLoading = true;
    __weak MovieListViewModel *weakSelf = self;
    [self.movieService searchForKeywordsWithQuery:query completion:^(NSArray<NSString *> *keywords) {
        if (weakSelf == nil || ![weakSelf.currentSearchQuery isEqualToString:query]) {
            return;
        }
        [weakSelf didLoadKeywords:keywords];
        weakSelf.isLoading = false;
    }];
}

#pragma mark - Setters

- (void)setContent:(NSArray<MovieListViewContentItem *> *)content {
    _content = content;
    if (self.onContentChanged) {
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.onContentChanged(content);
        });
    }
}

- (void)setIsLoading:(BOOL)isLoading {
    _isLoading = isLoading;
    if (self.onLoadingChanged) {
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.onLoadingChanged(isLoading);
        });
    }
}

- (void)setHasMorePages:(BOOL)hasMorePages {
    _hasMorePages = hasMorePages;
    if (self.onHasMorePagesChanged) {
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.onHasMorePagesChanged(hasMorePages);
        });
    }
}

- (void)setContentType:(MovieListViewModelContentType)contentType {
    _contentType = contentType;
    if (contentType == MovieListViewModelContentTypeNowPlaying) {
        self.content = self.playingNowContent;
        self.contentPage = self.playingNowContentPage;
    } else {
        self.content = @[];
        self.contentPage = 1;
    }
}

- (void)setCurrentSearchQuery:(NSString *)currentSearchQuery {
    BOOL didChange = ![_currentSearchQuery isEqualToString:currentSearchQuery];
    _currentSearchQuery = currentSearchQuery;
    if (didChange && onSearchQueryChanged) {
        onSearchQueryChanged(currentSearchQuery);
    }
}

@end

@implementation MovieListViewContentItem
@end
