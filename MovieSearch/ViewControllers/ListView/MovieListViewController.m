//
//  MovieListViewController.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 05/10/2020.
//

#import "MovieListViewController.h"
#import "MovieTableViewCell.h"
#import "KeywordTableViewCell.h"
#import <CCInfiniteScrolling/UIScrollView+infiniteScrolling.h>

@interface MovieListViewController ()
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *loadingView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UILabel *noResultsLabel;
@property (nonatomic, strong) NSArray<MovieListViewContentItem *> *content;
@end

@implementation MovieListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self bindToViewModel];
    [self.viewModel viewDidLoad];
    self.title = @"Movies";
    __weak MovieListViewController *weakSelf = self;
    [self.tableView addBottomInfiniteScrollingWithActionHandler:^() {
        [weakSelf.viewModel didToggleNextPage];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)bindToViewModel {
    __weak MovieListViewController *weakSelf = self;
    self.viewModel.onContentChanged = ^(NSArray<MovieListViewContentItem *> *newContent) {
        if (weakSelf == nil) {
            return;
        }
        NSArray<MovieListViewContentItem *> *oldContent = weakSelf.content;
        if (oldContent.count > 0 && newContent.count > oldContent.count && oldContent.firstObject.contentType == newContent.firstObject.contentType) {
            [weakSelf.tableView beginUpdates];
            NSMutableArray *indexPaths = [NSMutableArray new];
            for (NSInteger row = oldContent.count; row < newContent.count; row++) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:row inSection:0]];
            }
            [weakSelf.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            weakSelf.content = newContent;
            [weakSelf.tableView endUpdates];
        } else {
            weakSelf.content = newContent;
            [weakSelf.tableView reloadData];
        }
    };
    
    self.viewModel.onLoadingChanged = ^(BOOL isLoading) {
        if (weakSelf == nil || (isLoading && weakSelf.content != nil) || weakSelf.viewModel.contentType == MovieListViewModelContentTypeKeywords) {
            return;
        }
        [UIView animateWithDuration:0.3f animations:^(){
            if (!isLoading && self.content.count == 0 && self.viewModel.contentType == MovieListViewModelContentTypeSearch) {
                weakSelf.tableView.alpha = 0.0f;
                weakSelf.loadingView.alpha = 0.0f;
                weakSelf.noResultsLabel.alpha = 1.0f;
            } else {
                weakSelf.tableView.alpha = isLoading ? 0.0f : 1.0f;
                weakSelf.loadingView.alpha = isLoading ? 1.0f : 0.0f;
            }
        }];
    };
    
    self.viewModel.onImageFetched = ^(NSInteger itemIndex) {
        if (itemIndex >= [weakSelf.tableView indexPathsForVisibleRows].firstObject.row && itemIndex <= [weakSelf.tableView indexPathsForVisibleRows].lastObject.row) {
            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow: itemIndex inSection: 0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    };
    
    self.viewModel.onHasMorePagesChanged = ^(BOOL hasMorePages) {
        if (weakSelf == nil) {
            return;
        }
        weakSelf.tableView.infiniteScrollingDisabled = !hasMorePages;
    };
    
    self.viewModel.onMovieDetailsShouldBeDisplayed = ^(Movie *movie) {
        UIViewController *viewController = [weakSelf.movieDetailsBuilder movieDetailsViewControllerWithMovie:movie];
        [weakSelf.navigationController pushViewController:viewController animated:YES];
    };
    
    self.viewModel.onSearchQueryChanged = ^(NSString *searchQuery) {
        weakSelf.searchBar.text = searchQuery;
    };
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.viewModel.contentType == MovieListViewModelContentTypeKeywords) {
        return 40;
    }
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.content.count) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.viewModel didSelectItem:self.content[indexPath.row]];
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > self.content.count) {
        return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"NullCell"];
    }
    MovieListViewContentItem *item = self.content[indexPath.row];
    [self.viewModel willDisplayItem: item];
    if (item.contentType == MovieListViewModelContentTypeKeywords) {
        KeywordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KeywordTableViewCell"];
        [cell setupWithKeyword:item.keyword];
        return cell;
    } else {
        MovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MovieTableViewCell"];
        UIImage *logo = item.imageData != nil ? [UIImage imageWithData:item.imageData] : nil;
        [cell setupWithTitle:item.movie.title logo:logo isFavorite:item.movie.isFavorite];
        __weak MovieListViewController *weakSelf = self;
        cell.onFavoriteToggled = ^() {
            [weakSelf.viewModel didToggleFavorite:item];
        };
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.content.count;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = nil;
    [searchBar resignFirstResponder];
    [self.viewModel didTapCancelSearchButton];
    searchBar.showsCancelButton = false;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    searchBar.showsCancelButton = searchText.length > 0;
    [self.viewModel didEnterSearchQuery:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.viewModel didTapSearchButtonWithQuery:searchBar.text];
}

@end
