//
//  MovieListBuilder.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieListBuilder.h"
#import "MovieListViewController.h"
#import "MovieListViewModel.h"
#import "MovieService.h"
#import "MovieParser.h"
#import "MovieStorage.h"
#import "MovieDetailsBuilder.h"

@interface MovieListBuilder ()
@end

@implementation MovieListBuilder

- (UIViewController *)movieListViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MovieListViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MovieListViewController"];
    MovieService *movieService = [[MovieService alloc] initWithParser: [MovieParser new]];
    viewController.viewModel = [[MovieListViewModel alloc] initWithMovieService: movieService storage: [MovieStorage sharedInstance]];
    viewController.movieDetailsBuilder = [MovieDetailsBuilder new];
    return viewController;
}

@end
