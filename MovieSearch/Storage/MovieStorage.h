//
//  MovieStorage.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import "Movie.h"

@protocol MovieStorageProtocol <NSObject>

- (BOOL)isMovieFavorite:(Movie *)movie;
- (void)addMovieToFavorites:(Movie *)movie;
- (void)removeMovieFromFavorites:(Movie *)movie;
- (NSData *)imageDataWithMovie:(Movie *)movie;
- (void)saveimageData:(NSData *)data movie:(Movie *)movie;

@end

@interface MovieStorage : NSObject<MovieStorageProtocol>
+ (instancetype)sharedInstance;
@end
