//
//  MovieStorage.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 07/10/2020.
//

#import <Foundation/Foundation.h>
#import "MovieStorage.h"

const NSString *FavoriteMoviesDefaultsKey = @"favorite_movies";

@interface MovieStorage ()
@property (nonatomic, strong) NSArray<NSNumber *> *favorites;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, NSData *> *movieImages;
@end

@implementation MovieStorage

@synthesize favorites = _favorites;

+ (instancetype)sharedInstance {
    static MovieStorage *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MovieStorage alloc] init];
        sharedInstance.favorites = [[NSUserDefaults standardUserDefaults] arrayForKey:FavoriteMoviesDefaultsKey];
        if (!sharedInstance.favorites) {
            sharedInstance.favorites = @[];
        }
        sharedInstance.movieImages = [NSMutableDictionary new];
    });
    return sharedInstance;
}

- (BOOL)isMovieFavorite:(Movie *)movie {
    return [self.favorites containsObject:@(movie.movieID)];
}

- (void)addMovieToFavorites:(Movie *)movie {
    self.favorites = [self.favorites arrayByAddingObject:@(movie.movieID)];
    movie.isFavorite = true;
    [[NSUserDefaults standardUserDefaults] setValue:self.favorites forKey:FavoriteMoviesDefaultsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)removeMovieFromFavorites:(Movie *)movie {
    self.favorites = [self.favorites filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSNumber *object, NSDictionary* dictionary) {
        return movie.movieID != [object integerValue];
    }]];
    movie.isFavorite = false;
    [[NSUserDefaults standardUserDefaults] setValue:self.favorites forKey:FavoriteMoviesDefaultsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSData *)imageDataWithMovie:(Movie *)movie {
    return self.movieImages[@(movie.movieID)];
}

- (void)saveimageData:(NSData *)data movie:(Movie *)movie {
    self.movieImages[@(movie.movieID)] = data;
}

@end
