//
//  AppDelegate.m
//  MovieSearch
//
//  Created by Robert Bartoszewski on 05/10/2020.
//

#import "AppDelegate.h"
#import "MovieListBuilder.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    MovieListBuilder *listBuilder = [MovieListBuilder new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[listBuilder movieListViewController]];
    window = [UIWindow new];
    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
    return YES;
}

@end
