//
//  SceneDelegate.h
//  MovieSearch
//
//  Created by Robert Bartoszewski on 05/10/2020.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

